import json
from copy import deepcopy
from pprint import pprint

from src.configurations.dfs_name import *
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper
LOCAL = False
if LOCAL :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec"
else :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec-cluster"
dataname = "hopjs_db_v6_mismatching_distribution_hashed_ast_preprocessed_D1"
dataname = "NODEJS-final-preprocessed-renamedVariable-data-generator-V4-25800-with-HTML-D1"
db_version = "db-v6"
db_version = "db-v4"
root = "%s/dfs/%s/%s/" % (cluster, db_version, dataname)
json_file_path= "%sHOP_db-v6_valGlobalResults_100.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_200_to_500.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_500_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_500_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_final.json" % root



json_file_path= "%sHOP_db-v6_valGlobalResults_1300_and_3400.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_1300_and_2100.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_1300_2100_and_3400.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_1300_2100_and_3400.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_3400.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_3400_add_manually_500-210.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[10].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_[5500]_[10].json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_[5500]_[10].json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[20, 30, 50, 80, 130, 210].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_5500_merged.json" % root


json_file_path= "%sHOP_db-v6_testGlobalResults_100_to_800.json" % root
json_file_path_2= "%sHOP_db-v6_testGlobalResults_2100_to_5500.json" % root
json_path_merge= "%sHOP_db-v6_testGlobalResults_100_to_5500_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_5500_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[550].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_5500_with_[550]_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_5500_with_[550]_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[9780].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_5500_with_[550,9780]_merged.json" % root

json_file_path= "%sNODEJS_db-v4_valGlobalResults_only_[100, 200, 300, 500, 800, 1300, 2100, 3400]_[10, 20, 30, 50, 80, 130, 210, 550]_for_merge.json" % root
json_file_path_2= "%sNODEJS_db-v4_valGlobalResults_only_[5500]_[10, 20, 30, 50, 80, 130, 210, 550].json" % root
json_path_merge= "%sNODEJS_db-v4_valGlobalResults_100_to_5500_merged.json" % root

json_file_path= "%sNODEJS_db-v4_testGlobalResults_only_[100, 200, 300, 500, 800, 1300]_[10, 20, 30, 50, 80, 130, 210, 550]_clean_for_merge.json" % root
json_file_path_2= "%sNODEJS_db-v4_testGlobalResults_only_[2100, 3400, 5500]_[10, 20, 30, 50, 80, 130, 210, 550].json" % root
json_path_merge= "%sNODEJS_db-v4_testGlobalResults_100_to_5500_merged.json" % root

with open(json_file_path) as jsonFile:
    jsonObject = json.load(jsonFile)
jsonFile.close()


with open(json_file_path_2) as jsonFile:
    jsonObject_2 = json.load(jsonFile)
jsonFile.close()

print(jsonObject.keys())
print(jsonObject_2.keys())


def dict_of_dicts_merge(x, y):
    z = {}
    overlapping_keys = x.keys() & y.keys()
    for key in overlapping_keys:
        z[key] = dict_of_dicts_merge(x[key], y[key])
    for key in x.keys() - overlapping_keys:
        z[key] = deepcopy(x[key])
    for key in y.keys() - overlapping_keys:
        z[key] = deepcopy(y[key])
    return z

global_res = dict_of_dicts_merge(jsonObject, jsonObject_2)
pprint(global_res)
ResultDataframeFilesHelper.save_in_json_file(json_path_merge, global_res)