import json
import os
from pprint import pprint

from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper

LOCAL = False
if LOCAL :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec"
else :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec-cluster"

db_version = "db-v6"
db_version = "db-v4"
root = cluster + "/dfs/" + db_version + "/hopjs_db_v6_mismatching_distribution_hashed_ast_preprocessed_D1/"
root = cluster + "/dfs/" + db_version + "/NODEJS-final-preprocessed-renamedVariable-data-generator-V4-25800-with-HTML-D1/"
validation_or_test = "val"
validation_or_test = "test"
extra_message = "100_to_5500_merged"
language = "HOP"
language= "NODEJS"

json_name_path= "%s%s_%s_%sGlobalResults_%s.json" % (root, language, db_version, validation_or_test, extra_message)
if validation_or_test == "val":
    sheet_name = "VALIDATION"
else :
    sheet_name = "TEST"
dfs_path_json_result=root


list_maxContexts = [100, 200, 300, 500, 800,1300,2100,3400,5500]
list_maxPaths = [10, 20, 30, 50, 80, 130, 210,550,9780]
list_maxPaths = [10, 20, 30, 50, 80, 130, 210,550]
xlsx_path_name=os.path.join(str(dfs_path_json_result), language + "_" + db_version + "_" + "ClusterGlobalResults_" + extra_message + ".xlsx")
list_maxContexts_str =list(map(lambda i : str(i), list_maxContexts))
list_maxPaths_str =list(map(lambda i : str(i), list_maxPaths))
ResultDataframeFilesHelper.write_json_file_results_in_worksheet(json_name_path, list_maxContexts_str, list_maxPaths_str, xlsx_path_name, sheet_name, transpose_predicate=False)
with open(json_name_path) as jsonFile:
    jsonObject = json.load(jsonFile)
jsonFile.close()
pprint(jsonObject)