#!/usr/bin/env bash
###########################################################
# Change the following values to preprocess a new dataset.
# TRAIN_DIR, VAL_DIR and TEST_DIR should be paths to      
#   directories containing sub-directories with .java files
#   each of {TRAIN_DIR, VAL_DIR and TEST_DIR} should have sub-dirs,
#   and data will be extracted from .java files found in those sub-dirs).
# DATASET_NAME is just a name for the currently extracted 
#   dataset.                                              
# MAX_CONTEXTS is the number of contexts to keep for each 
#   method (by default 200).                              
# WORD_VOCAB_SIZE, PATH_VOCAB_SIZE, TARGET_VOCAB_SIZE -   
#   - the number of words, paths and target words to keep 
#   in the vocabulary (the top occurring words and paths will be kept). 
#   The default values are reasonable for a Tesla K80 GPU 
#   and newer (12 GB of board memory).
# NUM_THREADS - the number of parallel threads to use. It is 
#   recommended to use a multi-core machine for the preprocessing 
#   step and set this value to the number of cores.
# PYTHON - python3 interpreter alias.
WORD_VOCAB_SIZE=1000000
PATH_VOCAB_SIZE=1000000
TARGET_VOCAB_SIZE=1000000
NUM_THREADS=64
PYTHON=python3
LANG=NODEJS
db_version=db-v4
for maxtPath in 10 20 30 50 80 130 210 550
do
  for maxContexts in 100 200 300 500 800 2100 3400 5500
  do
    FILE_NAME=${LANG}${maxtPath}
    DATASET_NAME=NODEJS-final-preprocessed-renamedVariable-data-generator-V4-25800-with-HTML-D1
    EXTERNAL_DISK_PATH=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec
    DATASET_PAHT_NAME=${EXTERNAL_DISK_PATH}/db-raw-hashed-ast-detector/${db_version}/${FILE_NAME}
    TRAIN_DATA_FILE=${DATASET_PAHT_NAME}.train.raw.txt
    VAL_DATA_FILE=${DATASET_PAHT_NAME}.valid.raw.txt
    TEST_DATA_FILE=${DATASET_PAHT_NAME}.test.raw.txt
    ROOT_OUTPUT=${EXTERNAL_DISK_PATH}/db-c2v-hashed-ast-detector/${db_version}/${DATASET_NAME}/${LANG}-${maxtPath}-P_${maxContexts}-C
    OUTPUT_NAME=${ROOT_OUTPUT}/${LANG}-${maxtPath}-P_${maxContexts}-C
    echo ${OUTPUT_NAME};
    mkdir -p ${ROOT_OUTPUT}
    TARGET_HISTOGRAM_FILE=${OUTPUT_NAME}.histo.tgt.c2v
    ORIGIN_HISTOGRAM_FILE=${OUTPUT_NAME}.histo.ori.c2v
    PATH_HISTOGRAM_FILE=${OUTPUT_NAME}.histo.path.c2v
    echo ${TARGET_HISTOGRAM_FILE}

    echo "Creating histograms from the training data"
    cat ${TRAIN_DATA_FILE} | cut -d' ' -f1 | awk '{n[$0]++} END {for (i in n) print i,n[i]}' > ${TARGET_HISTOGRAM_FILE}
    cat ${TRAIN_DATA_FILE} | cut -d' ' -f2- | tr ' ' '\n' | cut -d',' -f1,3 | tr ',' '\n' | awk '{n[$0]++} END {for (i in n) print i,n[i]}' > ${ORIGIN_HISTOGRAM_FILE}
    cat ${TRAIN_DATA_FILE} | cut -d' ' -f2- | tr ' ' '\n' | cut -d',' -f2 | awk '{n[$0]++} END {for (i in n) print i,n[i]}' > ${PATH_HISTOGRAM_FILE}

    echo "Running preprocess"
#    OUTPUT_RES=${EXTERNAL_DISK_PATH}/db-c2v-hashed-ast-detector/${db_version}/${DATASET_NAME}/${LANG}-${maxtPath}-P_${maxContexts}-C/${LANG}-${maxtPath}-P_${maxContexts}-C
    ${PYTHON} preprocess.py --train_data ${TRAIN_DATA_FILE} --test_data ${TEST_DATA_FILE} --val_data ${VAL_DATA_FILE} --max_contexts $maxContexts --word_vocab_size ${WORD_VOCAB_SIZE} --path_vocab_size ${PATH_VOCAB_SIZE} --target_vocab_size ${TARGET_VOCAB_SIZE} --word_histogram ${ORIGIN_HISTOGRAM_FILE} --path_histogram ${PATH_HISTOGRAM_FILE} --target_histogram ${TARGET_HISTOGRAM_FILE} --output_name ${OUTPUT_NAME}

    # If all went well, the raw data files can be deleted, because preprocess.py creates new files
    # with truncated and padded number of paths for each example.
    rm ${TARGET_HISTOGRAM_FILE} ${ORIGIN_HISTOGRAM_FILE} ${PATH_HISTOGRAM_FILE}
  done
done


###########################################################


