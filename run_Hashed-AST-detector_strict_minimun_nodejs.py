import copy
import logging
from pathlib import Path

from src.configurations.dfs_name import *
from src.detectors.config import Config
from src.detectors.hashedASTDetector import Code2VecModel
from src.helpers.FolderHelper import FolderHelper
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper

list_maxContexts = [100, 200, 300, 500, 800]
list_maxPaths = [10, 20, 30, 50, 80, 130, 210]
list_maxContexts = [100]
list_maxPaths = [10]
# list_maxContexts = [100, 200]
# list_maxPaths = [10, 20]
"""
- On n'a pas les predictions rules
- On n'a pas les history plots
- On n'a pas le fichier excel final avec tous les resultats
"""
root_db_ml = Path("/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec/")
db_version = "db-v8"
root = Path(
    "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec/%s/" % db_version)
basePath=Path(root,"nodejs_db_v8_mismatching_distribution_with_percentage_hashed_ast_preprocessed_D2")
datasetName="node"
percent="0.11"

global_res = {DF_LOSS_TRAINING: {},
           DF_ACCURACY_TRAINING: {},
           DF_LOSS_EVALUATION: {},
           DF_ACCURACY_EVALUATION: {},
           DF_PRECISION_EVALUATION: {},
           DF_RECALL_EVALUATION: {},
           DF_F_MEASURED_EVALUATION: {},
           DF_TN_EVALUATION: {},
           DF_TP_EVALUATION: {},
           DF_FN_EVALUATION: {},
           DF_FP_EVALUATION: {},
           DF_WORD2VEC_TIME: {},
           DF_PADDING_TIME: {},
           DF_PARTITION_TIME: {},
           DF_KERAS_TRAINING_TIME: {},
           DF_KERAS_EVALUATION_TIME: {},
           DF_GLOBAL_TIME: {}
           }

for df in global_res:
    for maxContext in list_maxContexts:
        global_res[df][str(maxContext)] = {}
for maxContext in list_maxContexts:#100, 200, 300, 500, 800, 1300]:
    for maxPath in list_maxPaths:#10, 20, 30, 50, 80, 130, 210]:
        str_maxPath = str(maxPath)
        str_maxContext = str(maxContext)
        data=Path(basePath, percent +"pc", str_maxPath + datasetName + "-" + str_maxContext, str_maxPath + datasetName)
        # test=data+".test.c2v"#".val.c2v"
        test=str(Path(basePath, percent +"pc", str_maxPath + datasetName + "-" + str_maxContext, str_maxPath + datasetName + ".val.c2v"))
        save=Path(root,"models", datasetName, str_maxPath + "-" + str_maxContext, "saved_model")
        dfs_path=Path(root, "dfs", datasetName, datasetName + str_maxPath + "-" + str_maxContext)
        dfs_path_json_result=Path(root_db_ml, "dfs", db_version, datasetName + str_maxPath + "-" + str_maxContext)
        print("dfs_path_json_result -->" + str(dfs_path_json_result))
        FolderHelper.create_folder_if_not_exist(dfs_path_json_result)
        json_name_path = str(dfs_path_json_result) + percent + "percent" + "GlobalResults.json"
        config = Config(data,test,save,maxContext,maxPath, dfs_path, set_defaults=True, load_from_args=True, verify=True)
        model = Code2VecModel(config)
        config.log("Keras model -->" + str(model.keras_train_model))
        model.run_keras_fitting_process()
        config.log("HISTORY ABOUT TRAINING" + str(model.history))
        config.log(model.get_training_loss())
        config.log(model.get_training_accuracy())
        model.run_keras_evaluation_process()
        confusion_matrix_dict = model.run_measurement_process()
        # config.log(confusion_matrix_dict)
        nameOfile = config.data_path()
        indicesOfSlash = [i for i in range(len(nameOfile)) if nameOfile.startswith("/", i)]
        nameOfile = str(config.TRAIN_DATA_PATH_PREFIX) +"Results.json"
        ResultDataframeFilesHelper.save_in_json_file(nameOfile, confusion_matrix_dict)
        # config.log("CONFUSION MATRIX ==> " + str(confusion_matrix_dict))
        # config.log("GLOBAL RES BEFORE ==> " + str(global_res))
        for df in confusion_matrix_dict:
            # config.log("global_res[df][maxContext] = " + str(global_res[df][str_maxContext]))
            # config.log("confusion_matrix_dict[df][maxContext] = " + str(confusion_matrix_dict[df][str_maxContext]))
            global_res[df][str_maxContext].update(confusion_matrix_dict[df][str_maxContext])
            # config.log("global_res UPDATED" + str(global_res[df][str_maxContext]))
        # config.log("global_res UPDATED" + str(global_res))
ResultDataframeFilesHelper.save_in_json_file(json_name_path, global_res)
sheet_name = "VALIDATION"
xlsx_path_name=Path(root_db_ml, "dfs", db_version, datasetName + ".xlsx")
list_maxContexts_str =list(map(lambda i : str(i), list_maxContexts))
list_maxPaths_str =list(map(lambda i : str(i), list_maxPaths))
ResultDataframeFilesHelper.write_json_file_results_in_worksheet(json_name_path, list_maxContexts_str, list_maxPaths_str, xlsx_path_name, sheet_name)
