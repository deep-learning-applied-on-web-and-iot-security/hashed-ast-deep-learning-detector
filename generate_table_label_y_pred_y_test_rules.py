import json
import os
import re
from pathlib import Path

import logging
import multiprocessing

import numpy as np
import pandas as pd


#logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s",
#                        datefmt='%H:%M:%S', level=logging.WARNING)

######## WHAT IT'S NEED TO GENERATE THE TABLE LABEL, Y_PRED, Y_TEST, RULE
def append_df_to_excel(filename, df, sheet_name='Sheet1', startrow=None,
                       truncate_sheet=False,
                       **to_excel_kwargs):
    """
    Append a DataFrame [df] to existing Excel file [filename]
    into [sheet_name] Sheet.
    If [filename] doesn't exist, then this function will create it.

    Parameters:
      filename : File path or existing ExcelWriter
                 (Example: '/path/to/file.xlsx')
      df : dataframe to save to workbook
      sheet_name : Name of sheet which will contain DataFrame.
                   (default: 'Sheet1')
      startrow : upper left cell row to dump data frame.
                 Per default (startrow=None) calculate the last row
                 in the existing DF and write to the next row...
      truncate_sheet : truncate (remove and recreate) [sheet_name]
                       before writing DataFrame to Excel file
      to_excel_kwargs : arguments which will be passed to `DataFrame.to_excel()`
                        [can be dictionary]

    Returns: None
    """
    from openpyxl import load_workbook

    # ignore [engine] parameter if it was passed
    if 'engine' in to_excel_kwargs:
        to_excel_kwargs.pop('engine')

    writer = pd.ExcelWriter(filename, engine='openpyxl')

    # Python 2.x: define [FileNotFoundError] exception if it doesn't exist
    try:
        FileNotFoundError
    except NameError:
        FileNotFoundError = IOError


    try:
        # try to open an existing workbook
        writer.book = load_workbook(filename)

        # get the last row in the existing Excel sheet
        # if it was not specified explicitly
        if startrow is None and sheet_name in writer.book.sheetnames:
            startrow = writer.book[sheet_name].max_row

        # truncate sheet
        if truncate_sheet and sheet_name in writer.book.sheetnames:
            # index of [sheet_name] sheet
            idx = writer.book.sheetnames.index(sheet_name)
            # remove [sheet_name]
            writer.book.remove(writer.book.worksheets[idx])
            # create an empty sheet [sheet_name] using old index
            writer.book.create_sheet(sheet_name, idx)

        # copy existing sheets
        writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
    except FileNotFoundError:
        # file does not exist yet, we will create it
        pass

    if startrow is None:
        startrow = 0

    # write out the new sheet
    df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)

    # save the workbook
    writer.save()
def get_df_prediction_test_on(xlsx_path_file, sheet_name, nrows=6495, model=None) :
    if model == "construction" :
        skiprows = 1
        nrows = nrows
        usecols = np.arange(1, 5)  # +2 because of nodejs withe the extension max contexts
        index_col = 0
    else :
        skiprows = 1
        nrows = nrows
        usecols = np.arange(1, 10) #+2 because of nodejs withe the extension max contexts
        index_col = 0
    return pd.read_excel(xlsx_path_file, sheet_name=sheet_name, skiprows=skiprows, nrows=nrows, usecols=usecols, index_col=index_col, engine='openpyxl')

def generatePartitionAndLabelsAndRulesForADataSet(root_dataset_path, original_sanitization_path_json_file) :
    """

    :param root_dataset_path: can not be a relative path...
    :return:
    """
    print(root_dataset_path)
    tmp = str(root_dataset_path).split("/")
    nameDataset = tmp[-1]
    print("generation of the partition and labels for %s", nameDataset)
    resPartition = {nameDataset : []}
    resLabels = {}
    resRules = {}
    information_dataset = {nameDataset : {}}
    for root, name, files in os.walk(root_dataset_path):
        if ".DS_Store" in files :
            # logging.debug("REMOVE .DIRECTORY %s", files)
            files.remove(".DS_Store")
            # logging.debug("REMOVE .DIRECTORY %s", files)
        if ".directory" in files :
            # logging.debug("REMOVE .DIRECTORY %s", files)
            files.remove(".directory")
            # logging.debug("REMOVE .DIRECTORY %s", files)
        if os.path.join(nameDataset,'safe') in root :
            print("root" + root)
            files = list(map(lambda s : os.path.join(nameDataset,'safe', s.split(".")[0]), files))
            print("number of files in safe %d", len(files))
            resLabels.update(generateLabels(files, 1))
            resRules.update(generateRules(files, original_sanitization_path_json_file))
            print("number of labels in safe %d", resLabels.__len__())
            information_dataset[nameDataset]["safe"] = {"len" : len(files), "root" : os.path.join(nameDataset, 'safe'), "files" : files}
        elif os.path.join(nameDataset,'unsafe') in root :
            print("root" + root)
            files = list(map(lambda s: os.path.join(nameDataset,'unsafe', s.split(".")[0]), files))
            print("number of files in unsafe %d", len(files))
            resLabels.update(generateLabels(files, 0))
            print("number of labels in unsafe %d", resLabels.__len__())
            information_dataset[nameDataset]["unsafe"] = {"len" : len(files), "root" : os.path.join(nameDataset,'safe'), "files" : files}
            resRules.update(generateRules(files, original_sanitization_path_json_file))
        resPartition[nameDataset] += files
    return resPartition, resLabels, information_dataset, resRules

def generateLabels(files, safe) :
    #logging.debug(files)
    #logging.debug(safe)
    res = {}
    if safe :
        for f in files :
            res[f] = 0
    else :
        for f in files :
            res[f] = 1
    #logging.debug(res)
    return res


def find_the_sanitization_applied_from_the_file_name(string, original_sanitization_path_json_file):
    with open(original_sanitization_path_json_file) as f:
        original_sanitization_json_data = json.load(f)
        if "sanitization" in str(original_sanitization_path_json_file):
            predication_sanitization_file = True
        else :
            predication_sanitization_file = False
    for sanitization_name in original_sanitization_json_data :
        # logging.debug("current sanitization flush digit number--> " + sanitization_name)
        sanitization_name_flush = re.sub(r'-[0-9]+', '', sanitization_name)
        # logging.debug("current sanitization --> " + sanitization_name_flush)
        # logging.debug("current string --> " + string)
        if sanitization_name_flush in string :
            if predication_sanitization_file :
                if re.search("noFlow", string, re.IGNORECASE):
                    #print("no flow in --> %s ", string)
                    if not re.search("noFlow", sanitization_name_flush, re.IGNORECASE):
                        #print("NOT no flow in the --> %s HAVE TO CONTINUE TO SEARCH",sanitization_name_flush)
                        continue
            #print("FOUNDED --> %s in %s",sanitization_name_flush, string)
            return sanitization_name_flush, sanitization_name
    return None #Sanitization not found


def get_rules_from(original_sanitization_path_json_file, sanitization_name) :

    with open(original_sanitization_path_json_file) as f:
        original_sanitization_json_data = json.load(f)
    if "sanitization" in str(original_sanitization_path_json_file):
        predication_sanitization_file = True
        res = {
            "rule0": "0",
            "rule1": "0",
            "rule2": "0",
            "rule3": "0",
            "rule4": "0",
            "rule5": "0"
        }
    else:
        predication_sanitization_file = False
        res = {}
    if predication_sanitization_file :
        # logging.debug(original_sanitization_json_data[sanitization_name]["safety"])
        res["rule0"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule0")
        res["rule1"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule1")
        res["rule2"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule2")
        res["rule3"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule3")
        res["rule4"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule4")
        res["rule5"] = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule5")
    else :
        rule = try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule")
        res["rule"] = rule
    return res

def try_find_key_in_dict(dict, key) :
    try :
        res = dict[key]
    except KeyError as e :
        res = "0"
    return res

def find_the_rules_applied_from_the_file_name(string, original_sanitization_path_json_file) :
    sanitization_name_flush, sanitization_name = find_the_sanitization_applied_from_the_file_name(string, original_sanitization_path_json_file)
    logging.info( get_rules_from(original_sanitization_path_json_file, sanitization_name))
    return get_rules_from(original_sanitization_path_json_file, sanitization_name)

def generateRules(files, original_sanitization_path_json_file) :
    #logging.debug(files)
    #logging.debug(safe)
    res = {}
    for f in files :
        dict_rules = find_the_rules_applied_from_the_file_name(f, original_sanitization_path_json_file)
        res[f] = dict_rules
    #logging.debug(res)
    return res

def flatten_dict_rules(list_of_key_labels, dict_rules, sanitization_predicate=True) :
    if sanitization_predicate :
        res_rule0 = []
        res_rule1 = []
        res_rule2 = []
        res_rule3 = []
        res_rule4 = []
        res_rule5 = []
        for key_rules in dict_rules : #keyrule = name of the file associated to dict rule
            for key_label in list_of_key_labels : #key_label = name of the file associated to the label
                if key_rules in key_label :
                    res_rule0.append(dict_rules[key_rules]["rule0"])
                    res_rule1.append(dict_rules[key_rules]["rule1"])
                    res_rule2.append(dict_rules[key_rules]["rule2"])
                    res_rule3.append(dict_rules[key_rules]["rule3"])
                    res_rule4.append(dict_rules[key_rules]["rule4"])
                    res_rule5.append(dict_rules[key_rules]["rule5"])
        return res_rule0, res_rule1, res_rule2, res_rule3, res_rule4, res_rule5
    else :
        res_rule = []
        for key_rules in dict_rules:
            for key_label in list_of_key_labels:
                if key_rules in key_label:
                    res_rule.append(dict_rules[key_rules]["rule"])
        return res_rule


def savePredictionRegardingRules(root_dataset_path, kerasModel, testing_generator, path_prediction_excel_file_name, original_sanitization_path_json_file, sheet_name="PREDICTION") :
    _, resLabels, _, resRules = generatePartitionAndLabelsAndRulesForADataSet(root_dataset_path, original_sanitization_path_json_file)
    Y_pred = kerasModel.predict(testing_generator, workers=multiprocessing.cpu_count())
    y_pred = np.around(Y_pred).astype(int) #Transform into int (0 or 1)
    Y_TEST = np.concatenate([testing_generator[i][1] for i in range(len(testing_generator))])
    logging.debug("Shape of Y_pred (predited value by keras %s", Y_pred.shape)
    logging.debug("Shape of Y_TEST (expected value) %s", Y_TEST.shape)
    flatten = lambda l: [item for sublist in l for item in sublist]
    y_test_flatten = flatten(Y_TEST)
    y_pred_flatten = flatten(y_pred)
    res_rule0, res_rule1, res_rule2, res_rule3, res_rule4, res_rule5 = flatten_dict_rules(resLabels.keys(), resRules)
    dict_summarize = {"LABEL": list(resLabels.keys()), "Y_TEST": y_test_flatten, "Y_PRED": y_pred_flatten, "RULE0" : res_rule0, "RULE1" : res_rule1, "RULE2" : res_rule2 , "RULE3" : res_rule3, "RULE4" : res_rule4, "RULE5" : res_rule5}
    logging.warning(dict_summarize)
    df = pd.DataFrame.from_dict(dict_summarize)
    logging.warning(df)
    writer = pd.ExcelWriter(path_prediction_excel_file_name, engine='xlsxwriter')
    startRow = 1
    df.to_excel(writer, sheet_name=sheet_name, startrow=startRow)
    writer.save()
######## END OF  WHAT IT'S NEED TO GENERATE THE TABLE LABEL, Y_PRED, Y_TEST, RULE

######## WHAT IT'S NEED TO GENERATE THE TABLES OF CONFUSION MATRIX AND ALL METRIX, ACCURACY...

def getPredictionRegardingRules(root_dataset_path, kerasModel, testing_generator, original_sanitization_path_json_file):
    logging.debug(root_dataset_path)
    # root_dataset_path = Path(root_dataset_path)
    _, labels, _, resRules = generatePartitionAndLabelsAndRulesForADataSet(root_dataset_path,
                                                                           original_sanitization_path_json_file)
    logging.debug("RES RULES %s", resRules)
    Y_pred = kerasModel.predict(testing_generator)
    y_pred = np.around(Y_pred).astype(int)  # Transform into int (0 or 1)
    print("Shape of Y_pred (predited value by keras %s", Y_pred.shape)

    #Y_TEST = np.concatenate([testing_generator[i][1] for i in range(Y_pred.shape[0])])
    nuevaLista=[]
    for element in testing_generator:
        nuevaLista.extend(element[-1])
    Y_TEST=nuevaLista
    print("Shape of Y_TEST (expected value) %s", len(Y_TEST))
    flatten = lambda l: [item for sublist in l for item in sublist]
    y_test_flatten = Y_TEST#flatten(Y_TEST)
    y_pred_flatten = flatten(y_pred)
    print("y_test_flatten "+str(len(y_test_flatten)))
    print("y_pred_flatten "+str(len(y_pred_flatten)))

    if "sanitization" in str(original_sanitization_path_json_file) :
        sanitization_file_predicate = True
    else :
        sanitization_file_predicate = False
    if sanitization_file_predicate :
        res_rule0, res_rule1, res_rule2, res_rule3, res_rule4, res_rule5 = flatten_dict_rules(labels.keys(), resRules)
        dict_summarize = {"LABEL": list(labels.keys()), "Y_TEST": y_test_flatten, "Y_PRED": y_pred_flatten,
                          "RULE0": res_rule0, "RULE1": res_rule1, "RULE2": res_rule2, "RULE3": res_rule3,
                          "RULE4": res_rule4, "RULE5": res_rule5}
        logging.warning(len(list(labels.keys())))
        logging.warning(type((labels.keys())))
        logging.warning(len(y_test_flatten))
        logging.warning(type(y_test_flatten))
        logging.warning(len(y_pred_flatten))
        logging.warning(len(res_rule0))
        logging.warning(type(res_rule0))
        logging.warning(len(res_rule1))
        logging.warning(len(res_rule2))
        logging.warning(len(res_rule3))
        logging.warning(len(res_rule4))
        logging.warning(len(res_rule5))
    else :
        res_rule = flatten_dict_rules(labels.keys(), resRules, sanitization_predicate=False)
        # logging.warning("HEERREEEEEEEEE %s", res_rule)
        dict_summarize = {"LABEL": list(labels.keys()), "Y_TEST": y_test_flatten, "Y_PRED": y_pred_flatten,
                          "RULE": res_rule}

    print("res_rule "+str(len(res_rule)))
    print("labels "+str(len(labels)))
    # df = pd.DataFrame.from_dict(dict_summarize)
    # logging.warning(df)
    # return df
    return dict_summarize


def saveDFPredictionForTestingResults(df, XLSX, sheet_name) :
    df = pd.DataFrame.from_dict(df)
    if os.path.exists(XLSX) :
        append_df_to_excel(XLSX, df, sheet_name=sheet_name)
    else :
        writer = pd.ExcelWriter(XLSX, engine='openpyxl')
        startRow = 1
        df.to_excel(writer, sheet_name=sheet_name, startrow=startRow)
        writer.save()

def get_tp_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1) & (df_prediction["Y_PRED"] == 1)].shape[0]

def get_tn_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0) & (df_prediction["Y_PRED"] == 0)].shape[0]


def get_fp_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0) & (df_prediction["Y_PRED"] == 1)].shape[0]

def get_fn_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1) & (df_prediction["Y_PRED"] == 0)].shape[0]

def get_number_total_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number)].shape[0]

def get_number_total_safe_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0)].shape[0]

def get_number_total_unsafe_from(rule_number, df_prediction) :
    return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1)].shape[0]

def get_accuracy_from(rule_number, df_prediction) :

    tp = get_tp_from(rule_number, df_prediction)
    tn = get_tn_from(rule_number, df_prediction)
    total = get_number_total_from(rule_number, df_prediction)
    # logging.debug("tp=%s", tp)
    # logging.debug("tn=%s",tn)
    # logging.debug("total=%s",total)
    try :
        return (tp + tn)/ total
    except ZeroDivisionError :
        return 0

def get_recall_from(rule_number, df_prediction) :
    tp = get_tp_from(rule_number, df_prediction)
    fn = get_fn_from(rule_number, df_prediction)
    try :
        return tp/ (tp + fn)
    except ZeroDivisionError :
        return 0

def get_tnr_from(rule_number, df_prediction) :
    tn = get_tn_from(rule_number, df_prediction)
    fp = get_fp_from(rule_number, df_prediction)
    try:
        return tn/ (tn + fp)
    except ZeroDivisionError :
        return 0

def get_f_measure_from(rule_number, df_prediction) :
    precision = get_precision_from(rule_number, df_prediction)
    recall = get_recall_from(rule_number, df_prediction)
    try :

        return 2* precision * recall /(precision + recall)
    except ZeroDivisionError :
        return 0

def get_precision_from(rule_number, df_prediction) :
    tp = get_tp_from(rule_number, df_prediction)
    fp = get_fp_from(rule_number, df_prediction)
    try :
        return tp/ (tp + fp)
    except ZeroDivisionError :
        return 0


def get_dict_prediction_analyzer(xlsx, sheet_name, model="construction") :
    df_prediction = get_df_prediction_test_on(xlsx, sheet_name, model=model)

    for i in range(0, len(df_prediction)):
        df_prediction["Y_TEST"][i] = int(df_prediction["Y_TEST"][i][10:11])  # tf.Tensor(i, shape=(), dtype=int32) take i with i between 0,1]

    #print(df_prediction)
    # logging.debug(df_prediction)
    # logging.debug(df_prediction.index)  # Name file
    # logging.debug(df_prediction.columns)  # Y_TEST Y PRED RULE
    dict_res_confusion_matrix = {}
    dict_res_metricx = {}
    dict_totals = {}

    for rule_number in range(0,6) :
        number_of_rule = get_number_total_from(rule_number, df_prediction)
        total_safe = get_number_total_safe_from(rule_number, df_prediction)
        total_unsafe = get_number_total_unsafe_from(rule_number, df_prediction)
        number_of_rule_unsafe_predicted_unsafe_tp = get_tp_from(rule_number, df_prediction)
        number_of_rule_safe_predicted_safe_tn = get_tn_from(rule_number, df_prediction)
        number_of_rule_safe_predicted_safe_fp = get_fp_from(rule_number, df_prediction)
        number_of_rule_safe_predicted_safe_fn = get_fn_from(rule_number, df_prediction)
        key_rule = "rule"+str(rule_number)
        dict_res_confusion_matrix[key_rule] = {"TP": number_of_rule_unsafe_predicted_unsafe_tp,
                                               "TN": number_of_rule_safe_predicted_safe_tn,
                                               "FP": number_of_rule_safe_predicted_safe_fp,
                                               "FN": number_of_rule_safe_predicted_safe_fn,
                                               }
        dict_res_metricx[key_rule] = { "accuracy" : get_accuracy_from(rule_number, df_prediction),
                                       "precision" : get_precision_from(rule_number, df_prediction),
                                       "recall" : get_recall_from(rule_number, df_prediction),
                                       "TNR" : get_tnr_from(rule_number, df_prediction),
                                       "f_measure" : get_f_measure_from(rule_number, df_prediction),
                                       }
        dict_totals[key_rule] = {"total" : number_of_rule,
                                 "total_safe" :total_safe,
                                 "total_unsafe" : total_unsafe}
    #print(dict_res_confusion_matrix)
    return dict_res_confusion_matrix, dict_res_metricx, dict_totals


def get_df_prediction_analyzer(xlsx, sheet_name, model="construction") :
    dict_res_confusion_matrix, dict_res_metricx, dict_totals = get_dict_prediction_analyzer(xlsx=xlsx,
                                                                                            sheet_name=sheet_name,
                                                                                            model=model)
    df_confusion_matrix = pd.DataFrame.from_dict(dict_res_confusion_matrix)
    df_metrix = pd.DataFrame.from_dict(dict_res_metricx)
    df_total = pd.DataFrame.from_dict(dict_totals)
    return df_confusion_matrix, df_metrix, df_total


def save_df_prediction_analyzer( df_confusion_matrix, df_metrix, df_total, xlsx, sheet_name) :
    if os.path.exists(xlsx):
        append_df_to_excel(xlsx, df_confusion_matrix, sheet_name=sheet_name, startrow=2)
        append_df_to_excel(xlsx, df_metrix, sheet_name=sheet_name, startrow=9)
        append_df_to_excel(xlsx, df_total, sheet_name=sheet_name, startrow=19)
    else:
        writer = pd.ExcelWriter(xlsx, engine='openpyxl')
        startRow = 1
        df_confusion_matrix.to_excel(writer, sheet_name=sheet_name, startrow=startRow)
        df_metrix.to_excel(writer, sheet_name=sheet_name, startrow=9)
        df_total.to_excel(writer, sheet_name=sheet_name, startrow=19)
        writer.save()






