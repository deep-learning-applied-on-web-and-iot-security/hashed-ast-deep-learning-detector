import logging
from pathlib import Path

from src.detectors.config import Config
from generate_table_label_y_pred_y_test_rules import getPredictionRegardingRules, saveDFPredictionForTestingResults, \
    get_df_prediction_analyzer, save_df_prediction_analyzer
from src.helpers.FolderHelper import FolderHelper
from src.detectors.interactive_predict import InteractivePredictor
from src.detectors.keras_model import Code2VecModel

from src.detectors.path_context_reader import EstimatorAction
from src.detectors.vocabularies import VocabType


def runCode2Vec(data,test,save,maxContexts,dfs):
    logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s",
                        datefmt='%H:%M:%S', level=logging.INFO)
    config = Config(data,test,save,maxContexts,set_defaults=True, load_from_args=True, verify=True)

    model = Code2VecModel(config)
    config.log("MODEL TRAIN --> " + str(model.keras_train_model))
    config.log('Done creating code2vec model')
    if config.is_training:
        model.train()
    if config.SAVE_W2V is not None:
        model.save_word2vec_format(config.SAVE_W2V, VocabType.Token)
        config.log('Origin word vectors saved in word2vec text format in: %s' % config.SAVE_W2V)
    if config.SAVE_T2V is not None:
        model.save_word2vec_format(config.SAVE_T2V, VocabType.Target)
        config.log('Target word vectors saved in word2vec text format in: %s' % config.SAVE_T2V)
    if config.is_testing and not config.is_training:
        eval_results = model.evaluate()
        if eval_results is not None:
            config.log(str(eval_results).replace('topk', 'top{}'.format(config.TOP_K_WORDS_CONSIDERED_DURING_PREDICTION)))
    if config.PREDICT:
        predictor = InteractivePredictor(config, model)
        predictor.predict()

    #logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s", datefmt= '%H:%M:%S', level=logging.DEBUG)
    ######### WHAT YOU NEED TO COMPLETE BEFORE TO RUN THE SCRIPT
    ######## TO GENERATE THE TABLE LABEL, Y_PRED, Y_TEST, RULE

    # root_dataset_path= Path("/content/drive/MyDrive/code2VecExperiment/sourceCode/db-v6/NODEJS-final-preprocessed-data-v6-renamedVariable-with-r3-r4_11-template-ALL_IN_TRAINING__renamedVariable-with-r0-r1-r2-r5_14-template_SPLIT_IN_TEST_VALID-with-HTML-D1/validset_files") #Root dataset path to go to testset_file/
    root_dataset_path= Path("/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/ml-detector/db/db-v8/NODEJS-final-preprocessed-data-v8-renamedVariable-with-r3-r4_11-template-ALL_IN_TRAINING__renamedVariable-with-r0-r1-r2-r5_14-template-SPLIT_IN_TEST_VALID-with-HTML-D1-0.11pc/validset_files") #Root dataset path to go to testset_file/
    FolderHelper.create_folder_if_not_exist(dfs)
    path_prediction_excel_file_name=str(dfs)+"/"+dfs.stem+".xlsx" #change the name as you want
    kerasModel = model.keras_train_model #keras model already training to retrieve the y_pred values

    val_data_input_reader = model._create_data_reader(estimator_action=EstimatorAction.Evaluate)
    evaluationDataset = val_data_input_reader.get_dataset()

    testing_generator = evaluationDataset #Generator of the testing database to retrieve the Y_TEST values.
    sheet_name= "PREDICTION"
    #
    original_sanitization_path_json_file = Path("src/construction/original-construction.json") #TODO have to give to Santiago also
    #savePredictionRegardingRules(root_dataset_path,kerasModel,testing_generator,path_prediction_excel_file_name,sheet_name)
    # original_sanitization_path_json_file is the original-construction.json full path
    # root_dataset_path is the validset path for you
    # sheet_name is "PREDICTION"
    df_prediction = getPredictionRegardingRules(root_dataset_path, kerasModel, testing_generator, original_sanitization_path_json_file)
    print("df_prediction size : "+str(len(df_prediction)))
    saveDFPredictionForTestingResults(df_prediction, path_prediction_excel_file_name, sheet_name=sheet_name)

    ##################
    ##################
    ######### WHAT YOU NEED TO COMPLETE BEFORE TO RUN THE SCRIPT
    ######### TO GENERATE THE TABLES OF CONFUSION MATRIX AND ALL METRIX, ACCURACY...
    sheet_name_cm= "PREDICTION_CM"


    df_confusion_matrix, df_metrix, df_total = get_df_prediction_analyzer(xlsx=path_prediction_excel_file_name, sheet_name=sheet_name, model="construction")
    save_df_prediction_analyzer(df_confusion_matrix, df_metrix, df_total, xlsx=path_prediction_excel_file_name, sheet_name=sheet_name_cm)

    model.close_session()
root = Path("/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-hashed-ast-detector")
basePath=Path(root,"dataExperimentWithPercentages")
datasetName="node"
# datasetName="php-final"
#data="/content/drive/My Drive/code2VecExperiment/data/php-final10-100/php-final10"
#test="/content/drive/My Drive/code2VecExperiment/data/php-final10-100/php-final10.val.c2v"
#save="/content/drive/My Drive/code2VecExperiment/models/php-final10-100/saved_model"
percent="0.11"
MAX_PATH= "10"
MAX_CONTEXT= '300'
data=Path(basePath, percent +"pc", MAX_PATH + datasetName + "-" + MAX_CONTEXT, MAX_PATH + datasetName + "-" + MAX_CONTEXT)
data=Path(basePath, percent +"pc", MAX_PATH + datasetName + "-" + MAX_CONTEXT, MAX_PATH + datasetName)
# data=basePath+"/data/"+datasetName+"80-800/"+datasetName+"80"
test=str(Path(basePath, percent +"pc", MAX_PATH + datasetName + "-" + MAX_CONTEXT, MAX_PATH + datasetName + ".val.c2v"))
save=Path(root,"models", datasetName, MAX_PATH + "-" + MAX_CONTEXT, "saved_model")
dfs=Path(root,"dfs", datasetName, datasetName + MAX_PATH + "-" + MAX_CONTEXT)
runCode2Vec(data, test, save, int(MAX_CONTEXT), dfs)

# path_prediction_excel_file_name="/content/drive/My Drive/code2VecExperiment/data/php-final20-800/php-final20"+"TEST_LABELS_RULES"+".xlsx"
path_prediction_excel_file_name=Path(root, "dfs", datasetName, datasetName + MAX_PATH + "-" + MAX_CONTEXT + ".xlsx")
sheet_name_cm= MAX_PATH + "-" + MAX_CONTEXT+"PREDICTION_CM"
sheet_name= MAX_PATH + "-" + MAX_CONTEXT+"PREDICTION"
df_confusion_matrix, df_metrix, df_total = get_df_prediction_analyzer(xlsx=path_prediction_excel_file_name, sheet_name=sheet_name, model="construction")
save_df_prediction_analyzer(df_confusion_matrix, df_metrix, df_total, xlsx=path_prediction_excel_file_name, sheet_name=sheet_name_cm)

# for maxContexts in [100, 200, 300, 500, 800]:#100, 200, 300, 500, 800, 1300]:
#     for maxtPath in [ 10, 20, 30, 50, 80, 130, 210]:#10, 20, 30, 50, 80, 130, 210]:
#         data=basePath+"/data/10pc/"+str(maxtPath)+datasetName+"-"+str(maxContexts)+"/"+str(maxtPath)+datasetName
#         test=data+".test.c2v"#".val.c2v"
#         save=basePath+"/models/"+datasetName+str(maxtPath)+"-"+str(maxContexts)+"/"+"saved_model"
#         runCode2Vec(data,test,save,maxContexts)