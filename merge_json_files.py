import json
from pprint import pprint

from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper
LOCAL = False
if LOCAL :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec"
else :
    cluster = "/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/db-preprocess-code2vec-cluster"
root = "%s/dfs/db-v6/hopjs_db_v6_mismatching_distribution_hashed_ast_preprocessed_D1/" % cluster
json_file_path= "%sHOP_db-v6_valGlobalResults_100.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_200_to_500.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_500_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_500_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_final.json" % root



json_file_path= "%sHOP_db-v6_valGlobalResults_1300_and_3400.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_1300_and_2100.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_1300_2100_and_3400.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_800_merged.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_1300_2100_and_3400.json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_3400.json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_3400_add_manually_500-210.json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[10].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_[5500]_[10].json" % root

json_file_path= "%sHOP_db-v6_valGlobalResults_100_to_[5500]_[10].json" % root
json_file_path_2= "%sHOP_db-v6_valGlobalResults_only_[5500]_[20, 30, 50, 80, 130, 210].json" % root
json_path_merge= "%sHOP_db-v6_valGlobalResults_100_to_5500_merged.json" % root

json_file_path= "%sHOP_db-v6_testGlobalResults_100_to_800.json" % root
json_file_path_2= "%sHOP_db-v6_testGlobalResults_only_1300.json" % root
json_path_merge= "%sHOP_db-v6_testGlobalResults_100_to_1300_merged.json" % root

json_file_path= "%sHOP_db-v6_testGlobalResults_100_to_1300_merged.json" % root
json_file_path_2= "%sHOP_db-v6_testGlobalResults_2100_to_5500.json" % root
json_path_merge= "%sHOP_db-v6_testGlobalResults_100_to_5500_merged.json" % root

with open(json_file_path) as jsonFile:
    jsonObject = json.load(jsonFile)
jsonFile.close()

with open(json_file_path_2) as jsonFile:
    jsonObject_2 = json.load(jsonFile)
jsonFile.close()

# Python code to merge dict using a single
# expression
def Merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res

new_dict = dict()



for key in jsonObject :
    new_dict[key] = Merge(jsonObject[key], jsonObject_2[key])
#
# for key in jsonObject_2 :
#     new_dict[key] = Merge( jsonObject[key], jsonObject_2[key])
pprint(new_dict)
ResultDataframeFilesHelper.save_in_json_file(json_path_merge, new_dict)