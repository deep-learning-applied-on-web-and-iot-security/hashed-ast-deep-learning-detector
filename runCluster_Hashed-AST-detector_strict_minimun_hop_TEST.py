import copy
import logging
import os
from pathlib import Path

from src.configurations.dfs_name import *
from src.detectors.config import Config
from src.detectors.hashedASTDetector import Code2VecModel
from src.helpers.FolderHelper import FolderHelper
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper

# list_maxContexts = [100, 200, 300, 500, 800]
# list_maxPaths = [10, 20, 30, 50, 80, 130, 210]
# list_maxContexts = [1300, 2100, 3400, 5500]
# list_maxPaths = [10, 20, 30, 50, 80, 130, 210]
# list_maxContexts = [2100, 3400, 5500]
# list_maxPaths = [10, 20, 30, 50, 80, 130, 210]
list_maxContexts = [100, 200, 300, 500, 800, 1300, 2100, 3400, 5500]
list_maxPaths = [10, 20, 30, 50, 80, 130, 210, 550]
# list_maxContexts = [100]
# list_maxPaths = [10]
# list_maxContexts = [100, 200]
# list_maxContexts = [300]
# list_maxPaths = [10, 20]
"""
- On n'a pas les predictions rules
- On n'a pas les history plots
- On n'a pas le fichier excel final avec tous les resultats
"""
extr_message = "only_" + str(list_maxContexts) + "_" + str(list_maxPaths)
group = "/data/indes/user/hmaurel/DATA_DL"
root_db_ml = Path("%s/code2vec/db-preprocess-code2vec/" % group)
# db_version = "db-v6"
# db_version = "db-v8"
db_version = "db-v4"
root = Path(
    "%s/code2vec/db-preprocess-code2vec/%s/" % (group, db_version))
root = Path(
    "%s/code2vec/db-c2v-hashed-ast-detector/%s/" % (group, db_version))
# database_name = "hopjs_db_v6_mismatching_distribution_hashed_ast_preprocessed_D1"
# database_name = "nodejs_db_v8_mismatching_distribution_with_percentage_hashed_ast_preprocessed_D2"
database_name = "HOP-final-preprocessed-renamedVariable-data-generator-V4-34400-with-HTML-D1"
basePath=Path(root, database_name)
language= "HOP"
validation_or_test = "test"
# language= "node"
# percent=str(100)
# percent=str(0.11)


def runHashedDetector(list_maxContexts, list_maxPaths, root_db_ml, db_version, root, basePath, language, database_name, validation_or_test="val") :
    dfs_path_json_result=Path(root_db_ml, "dfs", db_version, database_name)

    FolderHelper.create_folder_if_not_exist(dfs_path_json_result)
    global_res = {DF_LOSS_TRAINING: {},
                  DF_ACCURACY_TRAINING: {},
                  DF_LOSS_EVALUATION: {},
                  DF_ACCURACY_EVALUATION: {},
                  DF_PRECISION_EVALUATION: {},
                  DF_RECALL_EVALUATION: {},
                  DF_F_MEASURED_EVALUATION: {},
                  DF_TN_EVALUATION: {},
                  DF_TP_EVALUATION: {},
                  DF_FN_EVALUATION: {},
                  DF_FP_EVALUATION: {},
                  DF_WORD2VEC_TIME: {},
                  DF_PADDING_TIME: {},
                  DF_PARTITION_TIME: {},
                  DF_KERAS_TRAINING_TIME: {},
                  DF_KERAS_EVALUATION_TIME: {},
                  DF_GLOBAL_TIME: {}
                  }

    for df in global_res:
        for maxContext in list_maxContexts:
            global_res[df][str(maxContext)] = {}
    for maxContext in list_maxContexts:#100, 200, 300, 500, 800, 1300]:
        for maxPath in list_maxPaths:#10, 20, 30, 50, 80, 130, 210]:
            str_maxPath = str(maxPath)
            str_maxContext = str(maxContext)
            context_path_folder_name = str_maxPath + language + "-" + str_maxContext # example 10node-100
            context_path_folder_name = language + "-" + str_maxPath + "-P_" + str_maxContext + "-C" # example HOP-10-P_100-C
            # data=Path(basePath, percent +"pc", context_path_folder_name, str_maxPath + language)
            data=Path(basePath,  context_path_folder_name, context_path_folder_name)
            # test=data+".test.c2v"#".val.c2v"
            # validation_or_test = "val"
            # test=str(Path(basePath, percent +"pc", context_path_folder_name, str_maxPath + language + "." + validation_or_test + ".c2v"))
            test=str(Path(basePath, context_path_folder_name, context_path_folder_name + "." + validation_or_test + ".c2v"))
            save=Path(root,"models", language, context_path_folder_name, "saved_model")
            dfs_path=Path(root, "dfs", language, context_path_folder_name)

            json_name_path = os.path.join(str(dfs_path_json_result), language + "_" + db_version + "_"+ validation_or_test + "GlobalResults_" + extr_message + ".json")
            config = Config(data,test,save,maxContext,maxPath, dfs_path, set_defaults=True, load_from_args=True, verify=True)
            model = Code2VecModel(config)
            config.log("Keras model -->" + str(model.keras_train_model))
            model.run_keras_fitting_process()
            config.log("HISTORY ABOUT TRAINING" + str(model.history))
            config.log(model.get_training_loss())
            config.log(model.get_training_accuracy())
            model.run_keras_evaluation_process()
            confusion_matrix_dict = model.run_measurement_process()
            # config.log(confusion_matrix_dict)
            nameOfile = str(config.TRAIN_DATA_PATH_PREFIX) + "_" + validation_or_test +"Results.json"
            ResultDataframeFilesHelper.save_in_json_file(nameOfile, confusion_matrix_dict)
            # config.log("CONFUSION MATRIX ==> " + str(confusion_matrix_dict))
            # config.log("GLOBAL RES BEFORE ==> " + str(global_res))
            for df in confusion_matrix_dict:
                # config.log("global_res[df][maxContext] = " + str(global_res[df][str_maxContext]))
                # config.log("confusion_matrix_dict[df][maxContext] = " + str(confusion_matrix_dict[df][str_maxContext]))
                global_res[df][str_maxContext].update(confusion_matrix_dict[df][str_maxContext])
                # config.log("global_res UPDATED" + str(global_res[df][str_maxContext]))
            # config.log("global_res UPDATED" + str(global_res))
            ResultDataframeFilesHelper.save_in_json_file(json_name_path, global_res)
    config.log("dfs_path_json_result -->" + str(dfs_path_json_result))
    if validation_or_test == "val" :
        sheet_name = "VALIDATION"
    else :
        sheet_name = "TEST"
    xlsx_path_name=os.path.join(str(dfs_path_json_result), language + "_" + db_version + "_" + "GlobalResults" + extr_message + ".xlsx")
    list_maxContexts_str =list(map(lambda i : str(i), list_maxContexts))
    list_maxPaths_str =list(map(lambda i : str(i), list_maxPaths))
    ResultDataframeFilesHelper.write_json_file_results_in_worksheet(json_name_path, list_maxContexts_str, list_maxPaths_str, xlsx_path_name, sheet_name)

# runHashedDetector(list_maxContexts, list_maxPaths, root_db_ml, db_version, root, basePath, language,database_name, validation_or_test="val")

runHashedDetector(list_maxContexts, list_maxPaths, root_db_ml, db_version, root, basePath, language, database_name, validation_or_test=validation_or_test)
