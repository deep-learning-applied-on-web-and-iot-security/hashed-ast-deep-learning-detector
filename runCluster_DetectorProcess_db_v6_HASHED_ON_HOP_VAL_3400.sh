#!/bin/bash

#source /etc/profile.d/modules.sh
#module load cuda/10.0
#module load cudnn/7.4-cuda-10.0
#
# Submission script for the helloWorld program
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# The job reserves 1 nodes with one processor (core) per node,
# Note : quoting style of parameters matters, follow the example
#OAR -p host!='nef005.inria.fr' and host!='nef002.inria.fr' and host!='nef004.inria.fr' and host!='nef003.inria.fr' and host!='nef006.inria.fr'
#OAR -l /core=12,walltime=720:00:00
#
#
# The job is submitted to the default queue
#OAR -q default
#
# Path to the binary to run
# source ./venv/bin/activate
venv/bin/python3 -m runCluster_Hashed-AST-detector_strict_minimun_hop_VALID_3400.py
#venv/bin/python3 -m src.main --env "local_debug_env.yaml"
