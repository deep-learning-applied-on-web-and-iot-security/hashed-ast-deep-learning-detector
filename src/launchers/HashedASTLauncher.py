import copy
from pprint import pprint

from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper
from src.launchers.Launchers import GenericLauncher


class HashedASTLauncher(GenericLauncher):
    def __init__(self, config):
        super().__init__(config)

    def setup(self, **kwargs):
        for k, v in kwargs.items() :
            setattr(self, k, v)
        pprint(self.__dict__)
        self.store_env = kwargs
        # for df in self.global_res:
        #     for embed in self.embedding_sizes:
        #         self.global_res[df][embed] = {}
        # self.dfs = ResultDataframeFilesHelper.createDataframes(len(self.list_df), index=self.embedding_sizes, columns=self.keras_layers)
        # self.dfs_test = ResultDataframeFilesHelper.createDataframes(len(self.list_df), index=self.embedding_sizes, columns=self.keras_layers)
        # self.global_test = copy.deepcopy(self.global_res)  # do it after setup_global_res