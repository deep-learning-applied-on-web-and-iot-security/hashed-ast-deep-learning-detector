import logging
import pickle
import re

from src.helpers.LoggingHelper import StaticLoggingHelper


class LineCodeHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)

    @classmethod
    def convertLineOfLongByteToToken(cls,line):
        return str(line).split("\\")

    @classmethod
    def convertLineByWord(cls,line):
        return re.split('(\W)', str(line), flags=re.UNICODE)

    @classmethod
    def apply_func_and_save_serialize_file(cls, fname, path_result, func, save_predicate=True):
        res = []
        cls.logger.debug("Open the file in binary read mode %s", fname)
        for line in open(fname, 'rb'):
            if len(line) != 1:
                res = res + func(line)
                cls.logger.debug("current res %s", res)
        cls.logger.debug("current res %s", res)
        if save_predicate:
            cls.logger.debug("Have to save --> %s", path_result)
            with open(path_result, 'wb') as fp:
                mon_pickler = pickle.Pickler(fp)
                mon_pickler.dump(res)
        return res
