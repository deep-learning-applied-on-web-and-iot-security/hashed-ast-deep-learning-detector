import glob
import logging
import os
import pickle
import re
from pathlib import Path
from threading import Thread

from src.helpers.FolderHelper import FolderHelper
from src.helpers.LineCodeHelper import LineCodeHelper
from src.helpers.LoggingHelper import StaticLoggingHelper


class TokenizationDatasetHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)

    @classmethod
    def createListTokensFor(cls, dirname, savePath, extension=re.compile(".*(\.php|\.js|\.zip|\.tar\.gz||\.tgz)"),
                        methodToConvertLineToListOfTokens=LineCodeHelper.convertLineOfLongByteToToken,
                        save_predicate=True):
        cls.logger.debug("Begining of create list token by threading for %s", dirname)
        list_dataset = glob.glob(os.path.join(dirname, '*', ''), recursive=False)
        cls.logger.debug("the list of train + test + valid set : %s", list_dataset)

        cls.logger.debug("Création des threads")
        cls.logger.debug("savePath %s", savePath)
        # Création des threads
        thread_1 = CreateListTokensForADataSet(list_dataset[0], savePath, extension, methodToConvertLineToListOfTokens,
                                               save_predicate)
        thread_2 = CreateListTokensForADataSet(list_dataset[1], savePath, extension, methodToConvertLineToListOfTokens,
                                               save_predicate)
        thread_3 = CreateListTokensForADataSet(list_dataset[2], savePath, extension, methodToConvertLineToListOfTokens,
                                               save_predicate)

        cls.logger.debug("Lancement des threads")
        # Lancement des threads
        thread_1.start()
        thread_2.start()
        thread_3.start()
        # Attend que les threads se terminent
        thread_1.join()
        thread_2.join()
        thread_3.join()
        if not save_predicate:  # Have to sure than you can load all the list of list of token in RAM
            list_of_list_tokens = thread_1.list_of_list_tokens + thread_2.list_of_list_tokens + thread_3.list_of_list_tokens
            return list_of_list_tokens
        else:
            return True

    @classmethod
    def createListTokensForADataset(cls,pathFile, pathResult, extension, methodToConvertLineToListOfTokens,
                                    savePredicate=True):
        cls.logger.debug("Current path processing %s", pathFile)
        FolderHelper.create_folder_if_not_exist(pathResult)
        cls.logger.debug("directory name --> %s", pathFile)
        list_files = glob.glob(os.path.join(os.path.join(pathFile, "*", "*.*")), recursive=True)
        filter_list_files = []
        acc = []
        for file in list_files:
            if extension.match(file):
                filter_list_files.append(file)
        cls.logger.debug("list of directory %s", filter_list_files)
        cls.logger.debug("len of list of directory %s", len(filter_list_files))
        p = re.compile(".*\.php")
        list = [s for s in filter_list_files if p.match(s)]
        cls.logger.debug("Number of %s files in the generator %d", p, len(list))
        p = re.compile(".*\.js")
        list = [s for s in filter_list_files if p.match(s)]
        cls.logger.debug("Number of %s files in the generator %d", p, len(list))
        p = re.compile(".*\.zip")
        list = [s for s in filter_list_files if p.match(s)]
        cls.logger.debug("Number of %s files in the generator %d", p, len(list))
        p = re.compile(".*\.tar\.gz")
        list = [s for s in filter_list_files if p.match(s)]
        cls.logger.debug("Number of %s files in the generator %d", p, len(list))
        p = re.compile(".*\.tgz")
        list = [s for s in filter_list_files if p.match(s)]
        cls.logger.debug("Number of %s files in the generator %d", p, len(list))

        # list = [os.path.getsize(s) for s in list_files]
        # cls.logger.info("NOT sorted files %s", list)
        filter_list_files = sorted(filter_list_files, key=lambda M: os.path.getsize(M))
        list = [os.path.getsize(s) for s in filter_list_files]
        cls.logger.debug("Size of the ascending sort files %s", list)
        cls.logger.debug("List of the ascending sort files %s", filter_list_files)
        for file in filter_list_files:
            cls.logger.debug("number of file that we need to parse --> %d", len(list_files))
            tmp = Path(file)
            binary_name_result = tmp.stem + '.bin'
            cls.logger.debug("Original name = " + os.path.join(pathResult, tmp.parts[-3], tmp.parts[-2], tmp.parts[-1]))
            cls.logger.debug("Binary  name = " + binary_name_result)
            path_result = Path(os.path.join(pathResult, tmp.parts[-3], tmp.parts[-2], binary_name_result))
            if not os.path.exists(path_result):
                cls.logger.debug("HAVE TO GENERATE AND SERIALIZE THE FILE %s AND THE SIZE OF THIS FILE %s",
                             binary_name_result, os.path.getsize(tmp))
                FolderHelper.create_folder_if_not_exist(os.path.join(pathResult, tmp.parts[-3], tmp.parts[-2]))

                res = LineCodeHelper.apply_func_and_save_serialize_file(file, path_result,
                                                                        methodToConvertLineToListOfTokens)
                if not savePredicate:
                    acc.append(res)
            else:
                cls.logger.debug("!!!!!!ALREADY SAVED - NOTHING TO DO --> %s ", binary_name_result)
                if not savePredicate:
                    if os.path.getsize(path_result) > 0:
                        with open(path_result, 'rb') as fp:
                            unpickler = pickle.Unpickler(fp);
                            res = unpickler.load()
                    else:
                        res = LineCodeHelper.apply_func_and_save_serialize_file(file, path_result,
                                                                                methodToConvertLineToListOfTokens)
                    acc.append(res)
        return acc




class CreateListTokensForADataSet(Thread):
    """"""

    def __init__(self, pathFile, pathResult, extension, methodToConvertLineToListOfTokens, savePredicate):
        Thread.__init__(self)
        self.pathFile = pathFile
        self.pathResult = pathResult
        self.extension = extension
        self.methodToConvertLineToListOfTokens = methodToConvertLineToListOfTokens
        self.savePredicate = savePredicate
        self.list_of_list_tokens = []


    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        self.list_of_list_tokens = TokenizationDatasetHelper.createListTokensForADataset(self.pathFile, self.pathResult, self.extension, self.methodToConvertLineToListOfTokens, self.savePredicate)

