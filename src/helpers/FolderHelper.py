import logging
import os

from src.helpers.LoggingHelper import StaticLoggingHelper


class FolderHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)


    @classmethod
    def create_folder_if_not_exist(cls, path) :
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)
            cls.logger.debug("%s doesn't exist ==> creation of this folder", path)
        else:
            cls.logger.debug("%s exist ==> do nothing", path)